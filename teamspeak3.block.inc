<?php
/*
 * Implements hook_block_info().
 */
function teamspeak3_block_info() {
  $blocks['teamspeak3_viewer'] = array(
    'info' => t('Teamspeak 3 Viewer'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}

/*
 * Implements hook_block_view().
 */
function teamspeak3_block_view($delta = '') {
  switch ($delta) {
    case 'teamspeak3_viewer':
      $ts3 = teamspeak3_create_server_instance(variable_get('teamspeak3_viewer_virtualserver', 1));
      if (!is_string($ts3)) {
        $block['subject'] = $ts3['virtualserver_name'];
        $block['content'] = theme('teamspeak3', teamspeak3_list_virtualserver($ts3));
      }
      else {
        $block['subject'] = 'Teamspeak Disconnected';
        $block['content'] = $ts3;
      }
  }
  return $block;
}

/*
 * Implements hook_block_configure().
 */
function teamspeak3_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case 'teamspeak3_viewer':
      $ts3 = teamspeak3_create_server_instance();
      $virtual_servers = array();
      foreach ($ts3->serverList() as $vs) {
        $info = $vs->getInfo();
        $virtual_servers[$info['virtualserver_id']] = $info['virtualserver_name'];
      }
      $form['teamspeak3_viewer_virtualserver'] = array(
        '#type' => 'radios',
        '#title' => 'Virtual Server to Display',
        '#default_value' => variable_get('teamspeak3_viewer_virtualserver'),
        '#options' => $virtual_servers,
      );
  }
  return $form;
}

/*
 * Implements hook_block_save().
 */
function teamspeak3_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 'teamspeak3_viewer':
      variable_set('teamspeak3_block_virtualserver', $edit['teamspeak3_viewer_virtualserver']);
  }
}