<?php
/*
 * @TODO: write docstring
 */
function teamspeak3_configuration_form($form, &$form_state) {
  $serverquery = variable_get('teamspeak3_serverquery', NULL);
  if ($serverquery == NULL) {
    $serverinfo['host'] = 'localhost';
    $serverinfo['port'] = '10011';
    $serverinfo['user'] = 'serveradmin';
    $serverinfo['pass'] = '';
  }
  else {
    $serverinfo = parse_url($serverquery);
  }
  
  
  $form['teamspeak_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connection Information'),
    '#weight' => 5,
    '#collapsible' => FALSE,
  );
  $form['teamspeak_config']['teamspeak3_config_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#default_value' => $serverinfo['host'],
    '#size' => 40,
    '#maxlength' => 80,
    '#description' => t('The address of the teamspeak server'),
    '#required' => TRUE,
  );
  $form['teamspeak_config']['teamspeak3_config_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port Number'),
    '#default_value' => $serverinfo['port'],
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('ServerQuery port.'),
    '#required' => TRUE,
  );
  $form['teamspeak_config']['teamspeak3_config_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => $serverinfo['user'],
    '#size' => 40,
    '#maxlength' => 80,
    '#description' => t('ServerQuery username.'),
    '#required' => TRUE,
  );
  $form['teamspeak_config']['teamspeak3_config_pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => $serverinfo['pass'],
    '#size' => 40,
    '#maxlength' => 80,
    '#description' => t('ServerQuery password.'),
    '#required' => TRUE,
  );
  $form['teamspeak_config']['teamspeak_config_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/*
 * Implements validation from the Form API.
 */
function teamspeak3_configuration_form_validate($form, &$form_state) {
  $host = $form_state['values']['teamspeak3_config_host'];
  $port = $form_state['values']['teamspeak3_config_port'];
  $user = $form_state['values']['teamspeak3_config_user'];
  $pass = $form_state['values']['teamspeak3_config_pass'];
  $serverquery = "serverquery://$user:$pass@$host:$port/";
  try {
    $ts3 = Teamspeak3::factory($serverquery);
  }
  catch (exception $e) {
    //form_set_error('teamspeak3_config_query_invalid', t('The information you have entered is invalid'));
    form_set_error('teamspeak3_config_query_invalid', $e->getMessage());
  }
}

/*
 * Implements submission from the Form API.
 */
function teamspeak3_configuration_form_submit($form, &$form_state) {
  //get the information from the form
  $host = $form_state['values']['teamspeak3_config_host'];
  $port = $form_state['values']['teamspeak3_config_port'];
  $user = $form_state['values']['teamspeak3_config_user'];
  $pass = $form_state['values']['teamspeak3_config_pass'];
  
  //generate and save the server query string
  $serverquery = "serverquery://$user:$pass@$host:$port/";
  variable_set('teamspeak3_serverquery', $serverquery);
}